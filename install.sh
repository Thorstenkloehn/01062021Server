sudo apt-get update
sudo apt-get upgrade
sudo apt-get install pandoc
apt-get install  gnupg
sudo apt-get install cmake gcc clang gdb build-essential git-core -y
sudo apt install python3 python3-dev git curl python-is-python3 -y
sudo apt-get install php php-cli php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-fpm -y
sudo apt-get install composer
sudo apt-get install r-base-dev
sudo apt-get install r-cran-evaluate
sudo composer global require daux/daux.io
cd /root
if [ -d /usr/local/go/ ] ; then
echo "Go ist Vorhanden"
else
wget https://golang.org/dl/go1.16.4.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf  go1.16.4.linux-amd64.tar.gz
echo "Go wird Installiert"
fi
export GOROOT=/usr/local/go
export GOPATH=/go
export PATH=$GOPATH/bin:$HOME/.cargo/bin:$GOROOT/bin:$PATH
cd /Server
git submodule update --init --recursive
cd /Server/Dokument
sudo /root/.composer/vendor/daux/daux.io/bin/daux generate
cd /Server/Programmiersprachen
sudo /root/.composer/vendor/daux/daux.io/bin/daux generate
cd /Server/Textverwaltung
sudo /root/.composer/vendor/daux/daux.io/bin/daux generate
cd /Server/Forschungswerkzeuge
sudo sudo /root/.composer/vendor/daux/daux.io/bin/daux generate