export GOROOT=/usr/local/go
export GOPATH=/go
export PATH=$GOPATH/bin:$HOME/.cargo/bin:$GOROOT/bin:$PATH
cd /Server
git pull
git submodule update --recursive --remote
cd /Server/Dokument
sudo /root/.composer/vendor/daux/daux.io/bin/daux generate
cd /Server/Programmiersprachen
sudo /root/.composer/vendor/daux/daux.io/bin/daux generate
cd /Server/Textverwaltung
sudo /root/.composer/vendor/daux/daux.io/bin/daux generate
cd /Server/Forschungswerkzeuge
sudo sudo /root/.composer/vendor/daux/daux.io/bin/daux generate