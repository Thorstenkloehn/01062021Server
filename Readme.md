# Server
## Installieren

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git-core
apt-get install  gnupg
git clone https://github.com/thorstenkloehn/Server.git /Server
cd /Server
```
* [Rust](https://www.rust-lang.org/tools/install)
* [caddy](https://caddyserver.com/docs/install)
```
bash install.sh

```

## Mysql
```
wget -c https://dev.mysql.com/get/mysql-apt-config_0.8.16-1_all.deb
sudo dpkg -i mysql-apt-config*
sudo apt update
sudo apt-get install mysql-server

```

## Servive erstellen Ubuntu

* sudo cp -u ahrensburg.service /etc/systemd/system/ahrensburg.service
* nano /etc/systemd/system/ahrensburg.service
```
[Unit]
Description=ahrensburg.digital

[Service]
WorkingDirectory=/Server/ahrensburg.digital
ExecStart=/Server/ahrensburg.digital/ahrensburg.digital
Restart=always
# ahrensburg-digital
RestartSec=10
KillSignal=SIGINT
SyslogIdentifier=ahrensburg.digital
User=root

[Install]
WantedBy=multi-user.target

```
## Server Dienst Aktivieren
``` 
sudo  systemctl enable ahrensburg.service

```

## Server Dienst Starten

```

sudo  systemctl start ahrensburg.service


```

## Python Installieren

``` 

sudo apt install python3 python3-dev git curl python-is-python3  python3-pip 
pip3 install ipython
pip3 install -U jupyter
jupyter
pip3 install -U  jupyterlab
jupyter-lab
pip3 install -U  notebook
jupyter notebook
pip3 install -U voila
```

## Apache Zeppelin

```

sudo apt-get install libfontconfig
sudo apt-get install r-base-dev
sudo apt-get install r-cran-evaluate
pip3 install jupyter
pip3 install grpcio
pip3 install protobuf

mkdir /zeppelin
wget https://downloads.apache.org/zeppelin/zeppelin-0.9.0/zeppelin-0.9.0-bin-all.tgz
sudo tar -C /zeppelin -xzf zeppelin-0.9.0-bin-all.tgz
cd /zeppelin/zeppelin-0.9.0-bin-all
cp conf/shiro.ini.template conf/shiro.ini
cd conf
nano shiro.ini
cd ..
bin/zeppelin-daemon.sh start

```
